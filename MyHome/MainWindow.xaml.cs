﻿using MyHome.Contracts.Common;
using MyHome.Contracts.Data;
using MyHome.Contracts.Services;
using MyHome.Database;
using MyHome.RealityClient.Interfaces;
using MyHome.RealityClient.SReality;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Threading;
using System.Net.Mail;
using System.Text;
using System.Net;
using System.IO;
using System.Diagnostics;
using Microsoft.Win32;

namespace MyHome
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        DispatcherTimer timer;
        List<IRealityClient> clients;
        DateTime updateStart;

        Queue<IRealityClient> clientsToUpdate;

        public MainWindow()
        {
            
            InitializeComponent();

            IocFactory.Container.Register<IDatabaseService>((o) => new MySQLDBFacade("MySQL"));

            timer = new DispatcherTimer(TimeSpan.FromHours(60), DispatcherPriority.Background, UpdateEvent, System.Windows.Application.Current.Dispatcher);

            clients = new List<IRealityClient>();
            clientsToUpdate = new Queue<IRealityClient>();

            clients.Add(new SRealityClient(browser));
            Initialize();

            InitQueries();
            
            Update();
            timer.Start();

            //SendAllRealities();
        }

        private void SendAllRealities()
        {
            List<Reality> newRealities = new List<Reality>();

            using (IDatabaseService db = IocFactory.Container.Resolve<IDatabaseService>())
            {
                newRealities = db.GetRealities().ToList();
            }
            SendEmailWithNews(newRealities);
        }

        private void InitQueries()
        {
            string query = "https://www.sreality.cz/hledani/prodej/domy/uherske-hradiste?region=m%C4%9Bstsk%C3%A1%20%C4%8D%C3%A1st%20Ma%C5%99atice&region-id=11649&region-typ=ward";

            using (IDatabaseService db = IocFactory.Container.Resolve<IDatabaseService>())
            {
                if (!db.GetRealityWebQueries().Any(q => q.Query == query))
                {
                    db.InsertRealityWebQuery(new RealityWebQuery() { Query = query, RealityWebId = clients[0].RealityWeb.ID });
                }
            }
        }

        private void Initialize()
        {
            foreach (IRealityClient client in clients)
            {
                client.Initialize();
                client.Updated += Client_Updated;
            }
        }

        private void Update()
        {
            lock (clientsToUpdate)
            {
                if (clientsToUpdate.Count > 0) return;
            }

            updateStart = DateTime.Now;
            clientsToUpdate = new Queue<IRealityClient>(clients);

            UpdateNextClient();

        }

        private void UpdateEvent(object sender, EventArgs args)
        {
            Update();
        }

        private void SendEmailWithNews(List<Reality> newRealities)
        {
            SmtpClient smtpClient = new SmtpClient();
            smtpClient.Port = 587;
            smtpClient.Host = "smtp.gmail.com";
            smtpClient.Credentials = new System.Net.NetworkCredential("moje.nove.bydleni@gmail.com", "TajneHeslo#852456");
            //smtpClient.UseDefaultCredentials = true;
            smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtpClient.EnableSsl = true;
            MailMessage mail = new MailMessage();
            
            //Setting From , To and CC
            mail.From = new MailAddress("moje.nove.bydleni@gmail.com", "MyHome");
            mail.To.Add(new MailAddress("michal.simara@gmail.com"));
            mail.CC.Add(new MailAddress("eviearch@seznam.cz"));
            mail.Subject = "MyHome - nové byty";

            mail.Body = CreateMailBody(newRealities);
            mail.BodyEncoding = Encoding.UTF8;
            mail.IsBodyHtml = true;

            smtpClient.Send(mail);
        }

        private string CreateMailBody(List<Reality> newRealities)
        {
            StringBuilder bodyBuilder = new StringBuilder();

            bodyBuilder.AppendLine("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
            bodyBuilder.AppendLine("<html xmlns=\"http://www.w3.org/1999/xhtml\">");
            bodyBuilder.AppendLine("<head><head/>");
            bodyBuilder.AppendLine("<body>");
            bodyBuilder.AppendLine("<div id=mail-header><b>Nové reality</b></div>");
            bodyBuilder.AppendLine("<div id=mail-body>");
            bodyBuilder.AppendLine("<ul id=realities-list>");
            foreach (Reality r in newRealities)
            {
                bodyBuilder.AppendFormat("<li class=\"reality-item\"><a href=\"{0}\" title=\"{1}\">{1}</a></li>", CompleteUri(r.Uri), r.Name);
                bodyBuilder.AppendLine();
            }
            bodyBuilder.AppendLine("</ul>");
            bodyBuilder.AppendLine("</div></body></html>");

            return bodyBuilder.ToString();
        }

        private object CompleteUri(string uri)
        {
            if(uri.StartsWith("https://"))
            {
                return uri;
            }

            return "https://" + uri;
        }

        private void Client_Updated()
        {
            UpdateNextClient();
        }

        private void UpdateNextClient()
        {
            lock (clientsToUpdate)
            {
                if (clientsToUpdate.Count == 0)
                {
                    FinalizeUpdate();
                }
                else
                {
                    IRealityClient client = clientsToUpdate.Dequeue();
                    client.Update();
                }
            }
        }

        private void FinalizeUpdate()
        {
            List<Reality> newRealities = new List<Reality>();
            using (IDatabaseService db = IocFactory.Container.Resolve<IDatabaseService>())
            {
                newRealities = db.GetRealities().Where(r => r.CreatedTime > updateStart).ToList();
            }
            if (newRealities.Count > 0)
            {
                SendEmailWithNews(newRealities);
            }
        }
       
    }
}
