﻿using MyHome.Contracts.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyHome.RealityClient.Interfaces
{

    public delegate void RealityUpdated();

    public interface IRealityClient
    {

        event RealityUpdated Updated;

        RealityWeb RealityWeb
        {
            get;
        }

        void Initialize();

        void Update();

    }
}
