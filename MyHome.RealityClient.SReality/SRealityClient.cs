﻿using MyHome.RealityClient.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Windows.Controls;
using System.Threading;
using System.Windows.Forms;
using System.Timers;
using System.Windows.Threading;
using MyHome.Contracts.Common;
using MyHome.Contracts.Services;
using MyHome.Contracts.Data;
using System.Text.RegularExpressions;
using System.IO;
using MyHome.Contracts.Enums;
using System.Web;
using System.Reflection;

namespace MyHome.RealityClient.SReality
{
    public class SRealityClient : IRealityClient
    {

        #region Fields

        public event RealityUpdated Updated;

        private System.Windows.Controls.WebBrowser browser;
        private DispatcherTimer timer;
        private RealityWeb web;
        private bool firstTick;
        private readonly object sync = new object();
        private Semaphore semaphore;

        #endregion

        public RealityWeb RealityWeb
        {
            get
            {
                return web;
            }
        }

        #region Constructors

        public SRealityClient()
        {
            browser = new System.Windows.Controls.WebBrowser();
            HideScriptErrors(browser, true);
            semaphore = new Semaphore(0, 1);
            timer = new DispatcherTimer(TimeSpan.FromMilliseconds(100), DispatcherPriority.ApplicationIdle, DownloadCheck, System.Windows.Application.Current.Dispatcher);
            timer.Stop();
            browser.Navigated += (s, e) => { timer.Start(); };
        }

        public SRealityClient(System.Windows.Controls.WebBrowser browser)
        {
            this.browser = browser;
            HideScriptErrors(browser, true);
            semaphore = new Semaphore(0, 1);
            timer = new DispatcherTimer(TimeSpan.FromMilliseconds(100), DispatcherPriority.ApplicationIdle, DownloadCheck, System.Windows.Application.Current.Dispatcher);
            timer.Stop();
            browser.Navigated += (s, e) => { timer.Start(); };
        }

        #endregion

        #region Methods

        public void HideScriptErrors(System.Windows.Controls.WebBrowser wb, bool hide)
        {
            var fiComWebBrowser = typeof(System.Windows.Controls.WebBrowser).GetField("_axIWebBrowser2", BindingFlags.Instance | BindingFlags.NonPublic);
            if (fiComWebBrowser == null) return;
            var objComWebBrowser = fiComWebBrowser.GetValue(wb);
            if (objComWebBrowser == null)
            {
                wb.Loaded += (o, s) => HideScriptErrors(wb, hide); //In case we are to early
                return;
            }
            objComWebBrowser.GetType().InvokeMember("Silent", BindingFlags.SetProperty, null, objComWebBrowser, new object[] { hide });
        }

        public void Initialize()
        {
            using (IDatabaseService db = IocFactory.Container.Resolve<IDatabaseService>())
            {
                web = db.GetRealityWebs().FirstOrDefault(w => w.Name == "SReality");
                if (web == null)
                {
                    web = new RealityWeb() { Name = "SReality", Status = Contracts.Data.DataObjectState.Active, WebUri = "www.sreality.cz" };
                    db.InsertRealityWeb(web);
                }
            }
        }

        public void Update()
        {
            Task.Factory.StartNew(() =>
            {
                using (IDatabaseService db = IocFactory.Container.Resolve<IDatabaseService>())
                {
                    List<RealityWebQuery> queries = db.GetRealityWebs().FirstOrDefault(w => w.Name == "SReality")?.Queries;
                    foreach (RealityWebQuery query in queries)
                    {
                        for (int page = 1; DownloadPage(query, page, db); page++) ;
                    }
                    Updated?.Invoke();
                }
            });
        }

        private bool DownloadPage(RealityWebQuery query, int page, IDatabaseService db)
        {
            firstTick = true;
            System.Windows.Application.Current.Dispatcher.Invoke(() =>
            {
                browser.Navigate(CreateQuery(query.Query, page));
            });

            semaphore.WaitOne();

            List<Reality> newRealities = new List<Reality>();
            int countOfPropertiesOnPage = 0;
            System.Windows.Application.Current.Dispatcher.Invoke(() =>
            {
                mshtml.IHTMLDocument3 htmlPage = browser.Document as mshtml.IHTMLDocument3;
                var elements = htmlPage.getElementsByTagName("div");
                for(int i = 0; i < elements.length; i++)
                {
                    mshtml.HTMLDivElement element = elements.item(i) as mshtml.HTMLDivElement;
                    if (element.className == "property ng-scope")
                    {
                        string propertyElement = element.outerHTML;
                        Reality r = ProcessProperty(propertyElement, db);
                        if(r != null)
                        {
                            newRealities.Add(r);
                        }
                        countOfPropertiesOnPage++;
                    }
                }
            });

            if (newRealities.Count > 0)
            {
                db.InsertReality(newRealities);
            }

            return newRealities.Count > 0;
        }

        private Reality ProcessProperty(string propertyElement, IDatabaseService db)
        {
            Regex regex = new Regex("href=\"(/detail/[^\"]*\\/([0-9]+))\""); // link
            Match match = regex.Match(propertyElement);
            if( match.Success)
            {
                string link = web.WebUri + match.Groups[1].Value;
                string sd = match.Groups[2].Value;
                if (!db.GetRealities().Any( r => r.Sd ==  sd))
                {
                    Regex regexName = new Regex("<span class=\"name ng-binding\">([^<]*)</span>"); // name
                    Match matchName = regexName.Match(propertyElement);
                    Regex regexPrice = new Regex("<span class=\"norm-price ng-binding\">([^<]*)Kč</span>"); // price
                    Match matchPrice = regexPrice.Match(propertyElement);

                    DateTime now = DateTime.Now;
                    string name = matchName.Success? HttpUtility.HtmlDecode(matchName.Groups[1].Value):String.Empty;
                    decimal? price = null;
                    if (matchPrice.Success)
                    {
                        decimal val;
                        if(Decimal.TryParse(HttpUtility.HtmlDecode(matchPrice.Groups[1].Value), out val))
                        {
                            price = val;
                        }
                    }
                    RealityType type = GetRealityType(link);

                    return new Reality()
                    {
                        CreatedTime = now,
                        LastUpdate = now,
                        Name = name,
                        Price = price,
                        RealityType = type,
                        RealityWebId = web.ID,
                        Uri = link,
                        Sd = sd
                    };       
                }
            }

            return null;
        }

        private RealityType GetRealityType(string link)
        {
            RealityType type = RealityType.Unknown;
            if (link.Contains("/byt/"))
            {
                type = RealityType.Apartment;
            } else if (link.Contains("/dum/"))
            {
                type = RealityType.House;
            } else if (link.Contains("/pozemek/")) {
                type = RealityType.Land;
            }
            return type;
        }

        private Uri CreateQuery(string query, int page)
        {
            Uri queryUri = new Uri(query);
            if(String.IsNullOrEmpty(queryUri.Query))
            {
                queryUri = new Uri(String.Format("{0}?strana={1}", query, page));
            } else if( queryUri.Query.Contains("strana=") )
            {
                Regex regex = new Regex("strana=(\\d*)");
                Match m = regex.Match(query);
                queryUri = new Uri(query.Replace(m.Value, String.Format("strana={0}", page)));
            } else
            {
                queryUri = new Uri(String.Format("{0}&strana={1}", query, page));
            }

            return queryUri;
        }

        private void DownloadCheck(object sender, EventArgs e)
        {
            System.Console.WriteLine("download check");
            lock (sync)
            {
                if (firstTick)
                {
                    dynamic doc = browser.Document;

                    dynamic script = doc.createElement("SCRIPT");
                    script.type = "text/javascript";
                    script.text = "function find(){ var elem = document.getElementsByClassName('dir-property-list'); return elem.length == 1; }";
                    dynamic head = doc.getElementsByTagName("head")[0];
                    if (head != null)
                    {
                        firstTick = false;
                        head.appendChild(script);
                    }
                    else
                    {
                        return;
                    }
                }
            }

            if (Convert.ToBoolean(browser.InvokeScript("find")))
            {
                timer.Stop();

                mshtml.IHTMLDocument3 page = browser.Document as mshtml.IHTMLDocument3;
                //if (page != null)
                //{
                //    PrintToFile("c:/Temp/sreality_inner.html", page.documentElement.innerHTML);
                //    PrintToFile("c:/Temp/sreality_outer.html", page.documentElement.outerHTML);
                //}

                semaphore.Release();
            }

        }

        private static void PrintToFile(string fileName, string data)
        {
            using (FileStream fs = new FileStream(fileName, FileMode.Create))
            {
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    sw.Write(data);
                    sw.Flush();
                }
            }
        }

        #endregion
    }
}





