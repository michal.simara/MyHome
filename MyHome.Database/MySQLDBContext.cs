﻿using MyHome.Contracts.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyHome.Database
{
    internal class MySQLDBContext : DbContext
    {

        #region Properties

        public DbSet<Reality> Realities { get; set; }

        public DbSet<RealityWeb> RealityWebs { get; set; }

        public DbSet<RealityWebQuery> RealityWebQueries { get; set; }

        #endregion

        #region Constructors

        public MySQLDBContext() : base()
        {

        }


        public MySQLDBContext(string nameOrConnectionString) : base(nameOrConnectionString)
        {

        }

        #endregion

    }
}
