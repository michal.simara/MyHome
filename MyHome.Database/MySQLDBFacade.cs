﻿using MyHome.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyHome.Contracts.Data;

namespace MyHome.Database
{
    public class MySQLDBFacade : IDatabaseService
    {

        #region Fields

        MySQLDBContext ctx;

        #endregion

        #region Constructors

        public MySQLDBFacade()
        {
            ctx = new MySQLDBContext();
        }


        public MySQLDBFacade(string connectionString)
        {
            ctx = new MySQLDBContext(connectionString);
        }

        #endregion

        #region Methods

        public IQueryable<Reality> GetRealities()
        {
            return ctx.Realities.Include("RealityWeb");
        }

        public IQueryable<RealityWeb> GetRealityWebs()
        {
            return ctx.RealityWebs.Include("Queries");
        }

        public void InsertReality(IEnumerable<Reality> realities)
        {
            ctx.Realities.AddRange(realities);
            ctx.SaveChanges();
        }

        public void InsertRealityWeb(RealityWeb realityWeb)
        {
            ctx.RealityWebs.Add(realityWeb);
            ctx.SaveChanges();
        }

        public void UpdateReality(Reality reality)
        {
            ctx.Entry(reality).State = System.Data.Entity.EntityState.Modified;
            ctx.SaveChanges();
        }

        public void UpdateRealityWeb(RealityWeb realityWeb)
        {
            ctx.Entry(realityWeb).State = System.Data.Entity.EntityState.Modified;
            ctx.SaveChanges();
        }

        public IQueryable<RealityWebQuery> GetRealityWebQueries()
        {
            return ctx.RealityWebQueries.Include("RealityWeb");
        }

        public void InsertRealityWebQuery(RealityWebQuery realityWebQuery)
        {
            ctx.RealityWebQueries.Add(realityWebQuery);
            ctx.SaveChanges();
        }

        public void UpdateRealityWebQuery(RealityWebQuery realityWebQuery)
        {
            ctx.Entry(realityWebQuery).State = System.Data.Entity.EntityState.Modified;
            ctx.SaveChanges();
        }



        public void Dispose()
        {
            ctx.Dispose();
        }

        #endregion

    }
}
