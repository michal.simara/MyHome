﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyHome.Contracts.Common
{

    public class IocFactory
    {
        public static IocContainer Container
        {
            get;
            set;
        }

        static IocFactory()
        {
            Container = new IocContainer();
        }
    }

    /// <summary>
    /// Container for storing instance depenencies
    /// </summary>
    public class IocContainer
    {
        #region Fields

        /// <summary>
        /// Stored factories for creating instances
        /// </summary>
        private Dictionary<ServiceKey, object> factories = new Dictionary<ServiceKey, object>();

        #endregion

        #region Register methods

        /// <summary>
        /// Registers the specified factory.
        /// </summary>
        /// <typeparam name="TService">The type of the service.</typeparam>
        /// <param name="factory">The factory.</param>
        public void Register<TService>(Func<IocContainer, TService> factory)
        {
            Register<TService, Func<IocContainer, TService>>(factory);
        }

        /// <summary>
        /// Registers the specified factory.
        /// </summary>
        /// <typeparam name="TService">The type of the service.</typeparam>
        /// <typeparam name="TArg">The type of the arg.</typeparam>
        /// <param name="factory">The factory.</param>
        public void Register<TService, TArg>(Func<IocContainer, TArg, TService> factory)
        {
            Register<TService, Func<IocContainer, TArg, TService>>(factory);
        }

        /// <summary>
        /// Registers the specified factory.
        /// </summary>
        /// <typeparam name="TService">The type of the service.</typeparam>
        /// <typeparam name="TArg1">The type of the arg1.</typeparam>
        /// <typeparam name="TArg2">The type of the arg2.</typeparam>
        /// <param name="factory">The factory.</param>
        public void Register<TService, TArg1, TArg2>(Func<IocContainer, TArg1, TArg2, TService> factory)
        {
            Register<TService, Func<IocContainer, TArg1, TArg2, TService>>(factory);
        }

        /// <summary>
        /// Registers the specified factory.
        /// </summary>
        /// <typeparam name="TService">The type of the service.</typeparam>
        /// <typeparam name="TArg1">The type of the arg1.</typeparam>
        /// <typeparam name="TArg2">The type of the arg2.</typeparam>
        /// <typeparam name="TArg3">The type of the arg3.</typeparam>
        /// <param name="factory">The factory.</param>
        public void Register<TService, TArg1, TArg2, TArg3>(Func<IocContainer, TArg1, TArg2, TArg3, TService> factory)
        {
            Register<TService, Func<IocContainer, TArg1, TArg2, TArg3, TService>>(factory);
        }

        /// <summary>
        /// Registers the specified factory.
        /// </summary>
        /// <typeparam name="TService">The type of the service.</typeparam>
        /// <typeparam name="TFactoryFunc">The type of the factory func.</typeparam>
        /// <param name="factory">The factory.</param>
        private void Register<TService, TFactoryFunc>(TFactoryFunc factory)
        {
            ServiceKey key = new ServiceKey(typeof(TService), typeof(TFactoryFunc));
            if (factories.ContainsKey(key))
            {
                factories.Remove(key);
            }
            factories.Add(key, factory);
        }

        #endregion

        #region Resolve methods

        /// <summary>
        /// Resolves this instance.
        /// </summary>
        /// <typeparam name="TService">The type of the service.</typeparam>
        /// <returns></returns>
        public TService Resolve<TService>()
        {
            return Resolve<TService, Func<IocContainer, TService>>(f => f(this));
        }

        /// <summary>
        /// Resolves the specified instance with specified argument.
        /// </summary>
        /// <typeparam name="TService">The type of the service.</typeparam>
        /// <typeparam name="TArg1">The type of the arg1.</typeparam>
        /// <param name="arg1">The arg1.</param>
        /// <returns></returns>
        public TService Resolve<TService, TArg1>(TArg1 arg1)
        {
            return Resolve<TService, Func<IocContainer, TArg1, TService>>(f => f(this, arg1));
        }

        /// <summary>
        /// Resolves the specified instance with specified arguments
        /// </summary>
        /// <typeparam name="TService">The type of the service.</typeparam>
        /// <typeparam name="TArg1">The type of the arg1.</typeparam>
        /// <typeparam name="TArg2">The type of the arg2.</typeparam>
        /// <param name="arg1">The arg1.</param>
        /// <param name="arg2">The arg2.</param>
        /// <returns></returns>
        public TService Resolve<TService, TArg1, TArg2>(TArg1 arg1, TArg2 arg2)
        {
            return Resolve<TService, Func<IocContainer, TArg1, TArg2, TService>>(f => f(this, arg1, arg2));
        }

        /// <summary>
        /// Resolves the specified instance with specified arguments
        /// </summary>
        /// <typeparam name="TService">The type of the service.</typeparam>
        /// <typeparam name="TArg1">The type of the arg1.</typeparam>
        /// <typeparam name="TArg2">The type of the arg2.</typeparam>
        /// <typeparam name="TArg3">The type of the arg3.</typeparam>
        /// <param name="arg1">The arg1.</param>
        /// <param name="arg2">The arg2.</param>
        /// <param name="arg3">The arg3.</param>
        /// <returns></returns>
        public TService Resolve<TService, TArg1, TArg2, TArg3>(TArg1 arg1, TArg2 arg2, TArg3 arg3)
        {
            return Resolve<TService, Func<IocContainer, TArg1, TArg2, TArg3, TService>>(f => f(this, arg1, arg2, arg3));
        }

        /// <summary>
        /// Resolves the specified invoker.
        /// </summary>
        /// <typeparam name="TService">The type of the service.</typeparam>
        /// <typeparam name="TFunc">The type of the func.</typeparam>
        /// <param name="invoker">The invoker.</param>
        /// <returns></returns>
        private TService Resolve<TService, TFunc>(Func<TFunc, TService> invoker)
        {
            var key = new ServiceKey(typeof(TService), typeof(TFunc));
            if (factories.ContainsKey(key))
            {
                TFunc factory = (TFunc)factories[key];
                return invoker(factory);
            }
            else
            {
                throw new NotSupportedException("Missing initializer for type: " + typeof(TService).Name + ", invoke function: " + typeof(TService).ToString());
            }
        }

        #endregion

        #region Contains

        /// <summary>
        /// Registers the specified factory.
        /// </summary>
        /// <typeparam name="TService">The type of the service.</typeparam>
        /// <param name="factory">The factory.</param>
        private bool ContainsKey<TService, TFactoryFunc>()
        {
            ServiceKey key = new ServiceKey(typeof(TService), typeof(TFactoryFunc));
            return factories.ContainsKey(key);
        }

        /// <summary>
        /// Determine of container contains the specified factory.
        /// </summary>
        /// <typeparam name="TService">The type of the service.</typeparam>
        /// <typeparam name="TArg">The type of the arg.</typeparam>
        public bool Contains<TService>()
        {
            return ContainsKey<TService, Func<IocContainer, TService>>();
        }

        /// <summary>
        /// Determine of container contains the specified factory.
        /// </summary>
        /// <typeparam name="TService">The type of the service.</typeparam>
        /// <typeparam name="TArg">The type of the arg.</typeparam>
        public bool Contains<TService, TArg>()
        {
            return ContainsKey<TService, Func<IocContainer, TArg, TService>>();
        }

        /// <summary>
        /// Determine of container contains the specified factory.
        /// </summary>
        /// <typeparam name="TService">The type of the service.</typeparam>
        /// <typeparam name="TArg1">The type of the arg1.</typeparam>
        /// <typeparam name="TArg2">The type of the arg2.</typeparam>
        public bool Contains<TService, TArg1, TArg2>()
        {
            return ContainsKey<TService, Func<IocContainer, TArg1, TArg2, TService>>();
        }

        /// <summary>
        /// Determine of container contains the specified factory.
        /// </summary>
        /// <typeparam name="TService">The type of the service.</typeparam>
        /// <typeparam name="TArg1">The type of the arg1.</typeparam>
        /// <typeparam name="TArg2">The type of the arg2.</typeparam>
        /// <typeparam name="TArg3">The type of the arg3.</typeparam>
        public bool Contains<TService, TArg1, TArg2, TArg3>()
        {
            return ContainsKey<TService, Func<IocContainer, TArg1, TArg2, TArg3, TService>>();
        }

        #endregion
    }
}
