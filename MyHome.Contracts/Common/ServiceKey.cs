﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyHome.Contracts.Common
{
    /// <summary>
    /// Model class for storing information about factory for given type
    /// </summary>
    public class ServiceKey
    {
        #region Properties

        /// <summary>
        /// Gets the type of the service.
        /// </summary>
        /// <value>
        /// The type of the service.
        /// </value>
        public Type ServiceType
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the type of the factory.
        /// </summary>
        /// <value>
        /// The type of the factory.
        /// </value>
        public Type FactoryType
        {
            get;
            set;
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceKey"/> class.
        /// </summary>
        /// <param name="serviceType">Type of the service.</param>
        /// <param name="factoryType">Type of the factory.</param>
        public ServiceKey(Type serviceType, Type factoryType)
        {
            this.ServiceType = serviceType;
            this.FactoryType = factoryType;
        }

        #endregion

        #region Overrides

        /// <summary>
        /// Determines whether the specified <see cref="System.Object"/> is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            ServiceKey key = obj as ServiceKey;
            if (key == null)
            {
                return false;
            }
            else
            {
                return (ServiceType == key.ServiceType && FactoryType == key.FactoryType);
            }
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            int shash = ServiceType.GetHashCode();
            int fhash = FactoryType.GetHashCode();
            return shash ^ fhash;
        }

        #endregion
    }
}
