﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyHome.Contracts.Data
{
    public class RealityWebQuery : DataObject
    {

        public String Query
        {
            get;
            set;
        }


        [ForeignKey("RealityWeb")]
        public int RealityWebId
        {
            get;
            set;
        }

        public RealityWeb RealityWeb
        {
            get;
            set;
        }

        public DateTime? LastUpdate
        {
            get;
            set;
        }

    }
}
