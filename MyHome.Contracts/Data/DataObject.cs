﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyHome.Contracts.Data
{

    public enum DataObjectState
    {
        Active,
        Deleted
    }

    public class DataObject
    {

        [Key]
        public int ID
        {
            get; set;
        }


        public DataObjectState Status
        {
            get;
            set;
        }

    }
}
