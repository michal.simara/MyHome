﻿using MyHome.Contracts.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyHome.Contracts.Data
{
    public class Reality : DataObject
    {

        public string Name
        {
            get;
            set;
        }

        public string Sd
        {
            get;
            set;
        }

        public string Uri
        {
            get;
            set;
        }

        public DateTime CreatedTime
        {
            get;
            set;
        }

        public DateTime LastUpdate
        {
            get;
            set;
        }

        public Decimal? Price
        {
            get;
            set;
        }

        public RealityType RealityType
        {
            get;
            set;
        }

        [ForeignKey("RealityWeb")]
        public int RealityWebId
        {
            get;
            set;
        }

        public RealityWeb RealityWeb
        {
            get;
            set;
        }

    }
}
