﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyHome.Contracts.Data
{
    public class RealityWeb : DataObject
    {

        public string Name
        {
            get;
            set;
        }

        public string WebUri
        {
            get;
            set;
        }

        public List<RealityWebQuery> Queries
        {
            get;
            set;
        }

        public RealityWeb()
        {
            Queries = new List<RealityWebQuery>();
        }

    }
}
