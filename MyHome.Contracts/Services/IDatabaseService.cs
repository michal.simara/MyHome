﻿using MyHome.Contracts.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyHome.Contracts.Services
{
    public interface IDatabaseService : IDisposable
    {

        #region Realities

        IQueryable<Reality> GetRealities();

        void InsertReality(IEnumerable<Reality> realities);

        void UpdateReality(Reality reality);

        #endregion

        #region Reality Web

        IQueryable<RealityWeb> GetRealityWebs();

        void InsertRealityWeb(RealityWeb realityWeb);

        void UpdateRealityWeb(RealityWeb realityWeb);

        #endregion

        #region Reality web query

        IQueryable<RealityWebQuery> GetRealityWebQueries();

        void InsertRealityWebQuery(RealityWebQuery realityWebQuery);

        void UpdateRealityWebQuery(RealityWebQuery realityWebQuery);

        #endregion

    }
}
