﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyHome.Contracts.Enums
{
    public enum ApartmanPropertyStatus
    {
        Unknown,
        VeryGood,
        Good,
        Poor,
        UnderConstruction,
        DeveloperProject,
        New,
        ToBeDemolished,
        ForRenovation,
        Renovated
    }
}
