﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyHome.Contracts.Common;
using MyHome.Database;
using MyHome.Contracts.Services;
using MyHome.Contracts.Data;

namespace MyHome.RealityClient.Tests
{
    [TestClass]
    public class SRealityClientTests
    {
        [TestMethod]
        public void GetData()
        {

            IocFactory.Container.Register<IDatabaseService>((o) => new MySQLDBFacade());
            SReality.SRealityClient client = new SReality.SRealityClient();
            client.Initialize();

            string query = "https://www.sreality.cz/hledani/prodej/byty/brno,brno-venkov?velikost=3%2Bkk,3%2B1";

            using (IDatabaseService db = IocFactory.Container.Resolve<IDatabaseService>())
            {
                if (!db.GetRealityWebQueries().Any(q => q.Query == query))
                {
                    db.InsertRealityWebQuery(new RealityWebQuery() { Query = query, RealityWebId = client.RealityWeb.ID });
                }
            }

            client.Update();
 
        }
    }
}
